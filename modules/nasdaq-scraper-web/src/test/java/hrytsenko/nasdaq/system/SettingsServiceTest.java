package hrytsenko.nasdaq.system;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SettingsServiceTest {

    private SettingsService service;

    @Before
    public void init() {
        service = new SettingsService();
        service.initSettings();
    }

    @Test
    public void testSettings() {
        Arrays.stream(SettingsService.class.getMethods()).filter(method -> method.getName().startsWith("get"))
                .filter(method -> !Objects.equals(method.getName(), "getClass")).forEach(method -> testSetting(method));
    }

    private void testSetting(Method method) {
        try {
            Assert.assertNotNull(method.invoke(service));
        } catch (Exception exception) {
            Assert.fail(exception.getMessage());
        }
    }

}
