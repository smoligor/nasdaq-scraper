package hrytsenko.nasdaq.system;

import hrytsenko.nasdaq.error.ApplicationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;

@Startup
@Singleton
@Lock(LockType.READ)
public class SettingsService {

    private static final Logger LOGGER = Logger.getLogger(SettingsService.class.getName());

    private static final String SETTINGS_FILE = "app.properties";

    private Map<String, String> settings;

    private static Map<String, String> loadSettings(String filename) {
        LOGGER.info(() -> String.format("Load settings from file %s.", filename));

        try {
            Properties properties = new Properties();
            byte[] content = Resources.toByteArray(Resources.getResource(filename));
            properties.load(new ByteArrayInputStream(content));
            return Maps.fromProperties(properties);
        } catch (IOException exception) {
            throw new ApplicationException("Could not read properties.", exception);
        }
    }

    @PostConstruct
    public void initSettings() {
        this.settings = Collections.unmodifiableMap(loadSettings(SETTINGS_FILE));
    }

    public String getLinkForDownloadByExchange() {
        return get("link_for_download_by_exchange");
    }

    public List<String> getExchanges() {
        return Splitter.on(',').trimResults().splitToList(get("exchanges"));
    }

    private String get(String name) {
        return settings.get(name);
    }

}
