package hrytsenko.nasdaq.daemon;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import hrytsenko.nasdaq.system.SettingsService;

@Startup
@Singleton
public class NasdaqDaemon {

    @Resource
    private TimerService timerService;

    @Inject
    private SettingsService settingsService;
    @Inject
    private NasdaqService nasdaqService;

    @PostConstruct
    public void initDaemon() {
        TimerConfig config = new TimerConfig(getClass().getSimpleName(), false);
        timerService.createSingleActionTimer(0, config);
        timerService.createCalendarTimer(new ScheduleExpression().hour("*").minute("*/30"), config);
    }

    @Timeout
    public void updateCompanies() {
        nasdaqService.updateCompanies(settingsService.getExchanges());
    }

}
