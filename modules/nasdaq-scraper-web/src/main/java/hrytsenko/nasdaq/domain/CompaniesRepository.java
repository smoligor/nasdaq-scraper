package hrytsenko.nasdaq.domain;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.domain.data.Company_;

@Stateless
public class CompaniesRepository {

    @PersistenceContext(unitName = "nasdaq-unit")
    private EntityManager entityManager;

    public List<Company> findCompanies(String exchange, String symbol, String sector) {
        return findCompanies(Optional.ofNullable(exchange), Optional.ofNullable(symbol), Optional.ofNullable(sector));
    }

    public void updateCompany(Company company) {
        Company updatedCompany = findCompanies(Optional.of(company.getExchange()), Optional.of(company.getSymbol()),
                Optional.empty()).stream().findFirst().orElse(company);

        updatedCompany.setName(company.getName());
        updatedCompany.setSector(company.getSector());
        updatedCompany.setSubsector(company.getSubsector());

        entityManager.merge(updatedCompany);
    }

    private List<Company> findCompanies(Optional<String> exchange, Optional<String> symbol, Optional<String> sector) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Company> query = builder.createQuery(Company.class);
        Root<Company> root = query.from(Company.class);
        query.select(root);

        query.where(builder.and(
                exchange.map(value -> builder.equal(root.get(Company_.exchange), value)).orElse(builder.conjunction()),
                symbol.map(value -> builder.equal(root.get(Company_.symbol), value)).orElse(builder.conjunction()),
                sector.map(value -> builder.equal(root.get(Company_.sector), value)).orElse(builder.conjunction())));

        return entityManager.createQuery(query).getResultList();
    }

    public List<String> findSectors() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<Company> root = query.from(Company.class);
        query.select(root.get(Company_.sector)).distinct(true).where(builder.notEqual(root.get(Company_.sector), "n/a"))
                .orderBy(builder.asc(root.get(Company_.sector)));

        return entityManager.createQuery(query).getResultList();
    }

}
